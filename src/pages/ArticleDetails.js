import React, { Component } from "react";
import ArticleService from '../services/Artices.services';


export default class ArticleDetails extends Component{
    state = {
        title: "",
        date: "",
        content: ""
    }

    async componentDidMount(){
        let {id} = this.props.match.params;
        console.log(id);
        let response = await ArticleService.getOneById(id);

        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.setState( data.article);
        }
        
    }

    render(){
        return(
            <div className="articlePreview" >
                <div className="apContainer">
                    <h1>{this.state.title}</h1>
                    <h3>Publié le : {this.state.date}</h3>
                    <div dangerouslySetInnerHTML={{__html: this.state.content}}></div>
                </div>  
            </div> 
        )
    }
}