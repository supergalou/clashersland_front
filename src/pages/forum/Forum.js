import React, { useEffect, useState, useContext } from "react";
import SubjectService from "../../services/Subject.service";
import SubjectPreview from "../../components/SubjectPreview";
import { Context } from '../../Context';

const Forum = () => {
  const {user, setUser} = useContext(Context);
  const [categorySelected, setSelectedCat] = useState("");
  const [search, setSearch] = useState("");
  const [categories, setCategories] = useState();
  const [subjects, setSubjects] = useState();

  useEffect(() => {
    // if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')!==null){
    //   setUser(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd'));
    // }
    getData();
  }, []);

  async function getData() {
    let response = await SubjectService.getSubjectCategories();
    if (response.ok) {
      let data = await response.json();
      setCategories(data.categories);
    }

    let responsesub = await SubjectService.getSubjects();
    if (responsesub.ok) {
      let datasub = await responsesub.json();
      setSubjects(datasub.subjects);
    }
  }

  async function searchSubjects(){
      console.log(categorySelected);
      
      let response = await SubjectService.searchSubjects(categorySelected, search);
      if (response.ok) {
        let data = await response.json();
        console.log(data);
        
        setSubjects(await data.subjects);
      }
  }

  return (
    <div>
      <h1 className="white">Sujet de discussion du forum</h1>

      <div className="filtresContainer">
        <h5>Filtrer les sujets de discussion</h5>

        <div className="filters">
          <div className="flex">
            <p className="filterLabel">Catégories : </p>
            <select className="sel" id="category" name="category" onChange={e => setSelectedCat(e.target.value)}>
              <option value="">Toutes</option>
              {categories !== null && categories !== undefined
                ? categories.map(item => {
                    return <option value={item._id}>{item.name}</option>;
                  })
                : null}
            </select>

            <p className="filterLabel">Recherche : </p>
            <input type="text" className="searchInput" onChange={(e) => setSearch(e.target.value)}></input>

            <button className="myButton" onClick={() => searchSubjects()}>Rechercher</button>
          </div>

          
        </div>
      </div>

      {subjects !== undefined && subjects.length > 0
        ? subjects.map(item => {
            return <SubjectPreview data={item} />;
          })
        : null}
    </div>
  );
};
export default Forum;
