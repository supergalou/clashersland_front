import React, {Component} from 'react';
import ArticleService from '../services/Artices.services';
import ArticlePreview from '../components/ArticlePreview';
import { Redirect } from 'react-router-dom';

export default class Blog extends Component {

    state = {
        articles: []
    }

    async componentDidMount(){
        let response = await ArticleService.getAll();
        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.setState({articles: data.articles});
        }
        
    }

    render(){
        return(
            <div>
                <h1 className="white">Bienvenue sur Casher's Land</h1>
                <h3 className="white">Voici les derniers articles parus :</h3>
                <div>
                    {
                        this.state.articles.length > 0 ?
                        this.state.articles.map(item=>{return(<ArticlePreview data={item}/>)})
                        : null
                    }
                </div>
            </div> 
        )
    }
}