import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Blog from './pages/Blog';
import './App.scss';
import Register from './pages/Register';
import Header from './components/Header';
import Login from './pages/Login';
import AddArticle from './pages/admin/AddArticle';
import AddSubjectCategory from './pages/admin/AddSubjectCategory';
import ArticleDetails from './pages/ArticleDetails';
import { StoreProvider } from './Context';
import MyAccount from './pages/MyAccount';
import AddSubject from './pages/forum/AddSubject';
import Forum from './pages/forum/Forum';
import SubjectDetails from './pages/forum/SubjectDetails';
import UserGestion from './pages/admin/UserGestion';



class App extends Component{

  render(){
    return(
      <BrowserRouter>
      <StoreProvider>
            <Header/>
            <div className="pageContainer">
              <main className="wrapper">
                <Route path="/" exact component={Blog}/>
                <Route path="/forum" exact component={Forum}/>
                <Route path="/forum/subject/:id" component={SubjectDetails}/>
                <Route path="/account" component = {MyAccount} />
                <Route path="/add-subject" component = {AddSubject} />
                <Route path="/user-gestion" component = {UserGestion} />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/add-article" component={AddArticle}/>
                <Route path="/add-subject-category" component={AddSubjectCategory}/>
                <Route path="/article/:id" component={ArticleDetails} />
              </main>
            </div>
          </StoreProvider>
      </BrowserRouter>
    )
  }
}

export default App;
