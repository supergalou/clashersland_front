import React, { useState } from 'react';


const Context = React.createContext({user: {}});

const StoreProvider = ({ children }) => {
    const [user, setUser] = useState(null);

    return(
        <Context.Provider value={{user, setUser}}>
            {children}
        </Context.Provider>
    );
}

export {StoreProvider, Context};